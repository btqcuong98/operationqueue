//
//  ApiRequest.swift
//  OperationQueueTest
//
//  Created by Bui Thai Quoc Cuong on 08/06/2021.
//

import Foundation
import UIKit

enum api: String {
    case store = "https://api.thecoffeehouse.com/api/get_list_store"
}

protocol ApiRequestProtocol {
    func request(_ complete: @escaping((States)->Void))
}

class ApiRequest: ApiRequestProtocol {
    func request(_ complete: @escaping((States)->Void)) {
        guard let url = URL(string: api.store.rawValue) else { return }
        let request = URLRequest(url: url)
        let task = URLSession(configuration: .default).dataTask(with: request) { (data,response,error) in
            if let data = data {
                do {
                    let results = try JSONDecoder().decode(States.self, from: data)
                    complete(results)
                } catch {
                    print("")
                }
            }
        }
        task.resume()
    }
}
