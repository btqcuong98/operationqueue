//
//  ListViewCell.swift
//  OperationQueueTest
//
//  Created by Bui Thai Quoc Cuong on 08/06/2021.
//

import UIKit

class ListViewCell: UITableViewCell {
    @IBOutlet weak private var imagePreview: UIImageView!
    @IBOutlet weak private var name: UILabel!
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setValue(_ image: UIImage, _ name: String) {
        self.imagePreview.image = image
        self.name.text = name
    }
    
    func setName(_ name: String) {
        self.name.text = name
    }
    
    func setImage(_ image: UIImage) {
        self.imagePreview.image = image
    }
    
    func startAnimation() {
        self.activityIndicator.startAnimating()
    }
    
    func stopAnimation() {
        self.activityIndicator.stopAnimating()
    }
    
    func hiddenAcitivityIndocator() {
        self.activityIndicator.isHidden = true
    }
}
