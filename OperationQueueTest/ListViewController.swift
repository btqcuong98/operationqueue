//
//  ListViewController.swift
//  OperationQueueTest
//
//  Created by Bui Thai Quoc Cuong on 08/06/2021.
//

import Foundation
import UIKit

final class ListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var vm: ListViewViewModel!
    private var photo: [PhotoRecord] = []
    private let pendingOperation = PendingOperations()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        register()
        vm = ListViewViewModel(service: ApiRequest())
        vm.refreshBlock = { indexPath in
            self.tableView.reloadRows(at: indexPath, with: .fade)
        }
        vm.reloadBlock = {
            self.tableView.reloadData()
        }
    }
    
    private func register() {
        self.tableView.register(UINib(nibName: "ListViewCell", bundle: nil), forCellReuseIdentifier: "ListViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
}

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListViewCell", for: indexPath) as? ListViewCell else {
            return UITableViewCell()
        }
        
        let photoRecord = vm.getPhotoRecord(indexPath)
        
        if let image = photoRecord.image {
            cell.setImage(image)
        }
        cell.setName(photoRecord.name)
        
        switch photoRecord.status {
        case .filtered:
            cell.stopAnimation()
            cell.hiddenAcitivityIndocator()
        case .failed:
            cell.stopAnimation()
            cell.hiddenAcitivityIndocator()
            cell.setName("Failed to load")
        case .downloaded, .new:
            cell.startAnimation()
            vm.startOperations(for: photoRecord, at: indexPath)
        }
        return cell
    }
}

extension ListViewController: UITableViewDelegate {
    
}
