//
//  ListViewViewModel.swift
//  OperationQueueTest
//
//  Created by Bui Thai Quoc Cuong on 12/06/2021.
//

import Foundation

class ListViewViewModel {
    private var service: ApiRequestProtocol!
    private var photos: [PhotoRecord] = []
    private var pendingOperation: PendingOperations!
    var refreshBlock: (([IndexPath]) -> Void)?
    var reloadBlock: (() -> Void)?
    
    init(service: ApiRequestProtocol) {
        self.service = service
        self.pendingOperation = PendingOperations()
        self.fetchData()
    }
    
    func fetchData() {
        service.request({ [weak self] response in
            let stores = response.states.flatMap {$0.districts.flatMap { $0.stores }}
            _ = stores.map {
                let photoRecord = PhotoRecord(name: $0.name, url: $0.images[0])
                self?.photos.append(photoRecord)
            }
            DispatchQueue.main.async {[weak self] in
                self?.reloadBlock?()
            }
        })
    }
    
    func numberOfRowsInSection() -> Int {
        return photos.count
    }
    
    func getPhotoRecord(_ indexPath: IndexPath) -> PhotoRecord {
        return self.photos[indexPath.row]
    }
    
    func startOperations(for photoRecord: PhotoRecord, at indexPath: IndexPath) {
        switch photoRecord.status {
        case .new:
            startDownload(for: photoRecord, at: indexPath)
        case .downloaded:
            startFilter(for: photoRecord, at: indexPath)
        default:
            print("do nothing")
        }
    }
    
    func startDownload(for photoRecord: PhotoRecord, at indexPath: IndexPath) {
        guard pendingOperation.downloadInProcess[indexPath] == nil else {
            return
        }
        let downloaded = ImageDownload(photoRecord)
        downloaded.main()
        //downloaded.completionBlock = {
            if downloaded.isCancelled {
                return
            }
            
            DispatchQueue.main.async {[weak self] in
                guard let self = self else { return }
                self.pendingOperation.downloadInProcess.removeValue(forKey: indexPath)
                self.refreshBlock?([indexPath])
            }
            
            self.pendingOperation.downloadInProcess[indexPath] = downloaded
            self.pendingOperation.downloadQueue.addOperation(downloaded)
        //}
    }
    
    func startFilter(for photoRecord: PhotoRecord, at indexPath: IndexPath) {
        guard pendingOperation.filterInProcess[indexPath] == nil else { return }
        let filterer = ImageFilter(photoRecord)
        filterer.main()
        filterer.completionBlock = {
            if filterer.isCancelled {
              return
            }
            
            DispatchQueue.main.async {[weak self] in
                self?.pendingOperation.filterInProcess.removeValue(forKey: indexPath)
                self?.refreshBlock?([indexPath])
            }
          }
          
        self.pendingOperation.filterInProcess[indexPath] = filterer
        self.pendingOperation.filterQueue.addOperation(filterer)
    }
}
