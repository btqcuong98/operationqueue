//
//  Model.swift
//  OperationQueueTest
//
//  Created by Bui Thai Quoc Cuong on 08/06/2021.
//

import Foundation

class States: Decodable {
    var states: [StatesDetail] = []
}

class StatesDetail: Decodable {
    var districts: [Districts] = []
}

class Districts: Decodable {
    var stores: [Stores] = []
}

class Stores: Decodable {
    var name: String = ""
    var images: [String] = []
}
