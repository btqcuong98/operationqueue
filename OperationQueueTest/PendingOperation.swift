//
//  PendingOperation.swift
//  OperationQueueTest
//
//  Created by BuiThaiQuocCuong on 6/8/21.
//

import Foundation

class PendingOperations {
    lazy var downloadInProcess: [IndexPath: Operation] = [:]
    lazy var downloadQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "Download queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    lazy var filterInProcess: [IndexPath: Operation] = [:]
    lazy var filterQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "Filteration queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}
