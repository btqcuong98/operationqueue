//
//  PhotoOperation.swift
//  OperationQueueTest
//
//  Created by BuiThaiQuocCuong on 6/8/21.
//

import Foundation
import UIKit

enum PhotoRecordStatus {
    case new, downloaded, filtered, failed
}

class PhotoRecord {
    var name: String = ""
    var url: String = ""
    var status: PhotoRecordStatus = PhotoRecordStatus.new
    var image: UIImage? = UIImage(named: "xxx")
    
    init(name: String, url: String) {
        self.name = name
        self.url = url
    }
}

class ImageDownload: Operation {
    var photoRecord: PhotoRecord?
    
    init(_ photoRecord: PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    override func main() {
        if isCancelled { return }
        guard let url = URL(string: photoRecord?.url ?? "") else { return }
        guard let imageData = try? Data(contentsOf: url) else { return }
        if isCancelled { return }
        if !imageData.isEmpty {
            photoRecord?.image = UIImage(data: imageData)
            photoRecord?.status = .downloaded
        } else {
            photoRecord?.image = UIImage(named: "Failed")
            photoRecord?.status = .failed
        }
    }
}

class ImageFilter: Operation {
    var photoRecord: PhotoRecord?
    
    init(_ photoRecord: PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    override func main() {
        if isCancelled { return }
        guard photoRecord?.status == .downloaded else { return }
        guard let image = photoRecord?.image else { return }
        photoRecord?.image = applySepiaFilter(image)
        photoRecord?.status = .filtered
    }
    
    func applySepiaFilter(_ image: UIImage) -> UIImage? {
        guard let data = image.pngData() else { return nil }
      let inputImage = CIImage(data: data)
          
      if isCancelled {
        return nil
      }
          
      let context = CIContext(options: nil)
          
      guard let filter = CIFilter(name: "CISepiaTone") else { return nil }
      filter.setValue(inputImage, forKey: kCIInputImageKey)
      filter.setValue(0.8, forKey: "inputIntensity")
          
      if isCancelled {
        return nil
      }
          
      guard
        let outputImage = filter.outputImage,
        let outImage = context.createCGImage(outputImage, from: outputImage.extent)
      else {
        return nil
      }

      return UIImage(cgImage: outImage)
    }

}
