//
//  Utilities.swift
//  OperationQueueTest
//
//  Created by Bui Thai Quoc Cuong on 08/06/2021.
//

import Foundation
import UIKit

class Utilities {
    static let shared = Utilities()
    
    func applySepiaFilter(image: UIImage) -> UIImage {
        let context = CIContext(options: nil)

        if let currentFilter = CIFilter(name: "CISepiaTone") {
            let beginImage = CIImage(image: image)
            currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
            currentFilter.setValue(0.5, forKey: kCIInputIntensityKey)

            if let output = currentFilter.outputImage {
                if let cgimg = context.createCGImage(output, from: output.extent) {
                    return UIImage(cgImage: cgimg)
                }
            }
        }
        return image
    }
}
